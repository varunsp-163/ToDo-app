import { useNavigate } from "react-router-dom";

import axios from "axios";
import { useEffect } from "react";

const Home = () => {
  const navigate = useNavigate();
  const getAllTodos = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL}/todos`
      );
      console.log("todos:", response.data);
    } catch (err) {
      console.error("Error fetching todos:", err);
    }
  };

  useEffect(() => {
    getAllTodos();
  }, []);

  return (
    <div className="text-center p-2">
      <button
        className="border-1 p-2 bg-red-400 rounded-md"
        onClick={() => navigate("/todos")}
      >
        Check out todo-app
      </button>
    </div>
  );
};

export default Home;
